//check env
var env = process.env.NODE_ENV || 'development';

//fetch enviroment config
var config = require('./config.json');
var envConfig = config[env];

//enum of all the data
Object.keys(envConfig).forEach(key => process.env[key] = envConfig[key]);