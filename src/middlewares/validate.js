const {validationResult} = require('express-validator');

module.exports = (req, res, next) => {
    const errors = validationResult(req);
    const {email} = req.body;
    console.log(req.body);
    console.log("Entered Veification middleware" + errors);
    if (!errors.isEmpty()) {
        console.log("Error");
        let error = {}; errors.array().map((err) => error[err.param] = err.msg);
        return res.status(422).json({error});
    }

    next();
};